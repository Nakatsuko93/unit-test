// test/example.test.js

const expect = require('chai').expect;
const should = require('chai').should();
const { assert } = require('chai');
const mylib= require('../src/mylib');

describe('Unit testing mylib.js', () => {

    let myvar = undefined;

    before(() => {
        myvar = 1; // setup before testing
        console.log('Before testing...');
    })

    it('Myvar should exist', () => {
        should.exist(myvar);
    })

    it('Should return 2 when using sum function with a=1, b=1', () => {
        const result = mylib.sum(1,1); // 1 + 1
        expect(result).to.equal(2); // result expected to equal 2
    });

    it('parametrized way of unit testing', () => {
        const result = mylib.sum(myvar, myvar); // 1 + 1
        expect(result).to.equal(myvar+myvar);
    })

    it('Assert foo is not bar', () => {
        assert('foo' !== 'bar') // true
    })

    after(() => {
        console.log('After testing...');
    })
})